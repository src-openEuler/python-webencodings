%bcond_without python3

Name: python-webencodings
Version: 0.5.1
Release: 9
Summary: Character encoding for the web
License: BSD
URL: https://github.com/gsnedders/python-webencodings
BuildArch: noarch

Source0: %{url}/archive/v%{version}.tar.gz


BuildRequires: python3-devel
BuildRequires: python3-pytest
BuildRequires: python3-sphinx

%description
This is a Python implementation of the WHATWG Encoding standard.

%package help
Summary: Documentation for python-webencodings
Provides: python-webencodings-doc
Obsoletes: python-webencodings-doc

%description help
Documentation for python-webencodings.


%package -n python3-webencodings
Summary: %{summary}

%{?python_provide:%python_provide python3-webencodings}

Requires: python3

%description -n python3-webencodings
This is a Python3 implementation of the WHATWG Encoding standard.

%prep
%autosetup -n python-webencodings-%{version}

%build
%py3_build

PYTHONPATH=. sphinx-build-3 docs docs/_build

rm -rf docs/_build/.buildinfo
rm -rf docs/_build/.doctrees

%install
%py3_install


%check
py.test-3

%files help
%doc docs/_build

%files -n python3-webencodings
%license LICENSE
%doc README.rst
%{python3_sitelib}/webencodings
%{python3_sitelib}/*.egg-info

%changelog
* Thu Oct 29 2020 tianwei <tianwei12@huawei.com> - 0.5.1-9
- delete python2

* Tue Sep 8 2020 shixuantong <shixuantong@huawei.com> - 0.5.1-8
- update Source0

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.5.1-7
- Package init
